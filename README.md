ScreenshotBASH Endpoint for PHP
-------------------------------
Upload endpoint for the [ScreenshotBASH](https://gitlab.com/Scrumplex/ScreenshotBASH) project written in PHP.

# Installation
 - Build and run this container
 - Or download container image from this repo's container registry and run it.

# Important Docker paths
 - `/var/www/html/config.php`
 - `/var/www/html/{img,watch,cloud}`

# License
This project is licensed under the GNU General Public License v3.
You can find more information about it in the [LICENSE](LICENSE) file.
