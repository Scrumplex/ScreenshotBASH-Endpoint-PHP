<?php
$config = array(

    // A password to prevent other uploads.
    "password" => "your_password",

    // List of mime-types and their corresponding target folders (relative to the location of this script)
    "targetDirectories" => array(
        "image/*" => "img", // folder for type image/*
        "video/*" => "watch", // folder for type video/*
        "*" => "cloud" // folder for any other type
    ),
    "banned_exts" => array(
        ".php"
    ),
    "output_url" => "https://example.com" // The url of the parent folder of this script
);
