<?php
/*
 *   ScreenshotBASH-Endpoint-PHP
 *   Copyright (C) 2021 Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

$bad_chars = array("/", "?", "#", "\"", "'", "`", "´", "&");


header("Content-Type", "text/plain; charset=UTF-8");

// Load config
require_once __DIR__ . "/config.inc.php";

// Create directories if needed
foreach ($config["targetDirectories"] as $targetDirectory) {
    if (!file_exists($targetDirectory))
        mkdir($targetDirectory, 0644, true);
}

// only if authenticated
if (empty($config["password"]) ||
    isset($_POST["password"]) && $_POST["password"] == $config["password"]) {

    $file = $_FILES["upload"];

    // Check for banned extensions
    foreach ($config["banned_exts"] as $ext) {
        if (str_ends_with($file["name"], $ext)) {
            http_send_status(400);
            die("ext_disallowed");
        }
    }

    // Figure out target folder
    $targetDirectory = "";
    foreach (array_keys($config["targetDirectories"]) as $mime) {
        if (fnmatch($mime, $file["type"])) {
            $targetDirectory = $config["targetDirectories"][$mime];
            $targetDirectory = rtrim($targetDirectory, "/"); // Remove trailing slash(es)
            break;
        }
    }

    // Sanitize filename
    $filename = str_replace($bad_chars, "_", $file["name"]);
    if (empty($filename))
        $filename = uniqid(); // random name if there is no filename available

    // Add a prefix if file already exists
    $i = 1;
    $target = join("/", array(__DIR__, $targetDirectory, $filename));
    $url = join("/", array($config["output_url"], $targetDirectory, $filename));

    while (file_exists($target)) {
        $newFilename = $i . "_" . $filename;
        $target = join("/", array(__DIR__, $targetDirectory, $newFilename));
        $url = join("/", array($config["output_url"], $targetDirectory, $newFilename));
    }

    // Move uploaded file to target
    if (move_uploaded_file($file["tmp_name"], $target)) {
        die($url);
    }

    die("Error");
}
die("Unauthorized");
